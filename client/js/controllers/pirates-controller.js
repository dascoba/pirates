piratesApp.controller('piratesController', ['$scope', '$http', function($scope, $http) {

  $scope.processing = false;
  $scope.numberOfPirates = 2;
  $scope.results = [];

  var piratesApi = 'http://pirate.azurewebsites.net/api/Pirate/';

  $scope.getTreasureCount = function(){
    var requestIndex = $scope.results.length + 1;
    $scope.processing = true;
    $http.get(piratesApi + $scope.numberOfPirates)
      .success(function(data){
        $scope.results.push(requestIndex + ' : Treasure: ' + data);
        $scope.processing = false;
      })
      .error(function(err){
        var suggestion = '';
        if($scope.numberOfPirates > 1)
        {
          suggestion = 'Enter a value less than ' + $scope.numberOfPirates;
        }
        $scope.results.push(requestIndex + ' : ' + err.Message + ' ' + suggestion);
        $scope.processing = false;
      });
  };

}]);
