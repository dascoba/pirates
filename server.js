var express         = require('express'),
    bodyParser      = require('body-parser'),
    app             = express();

// middleware
app.use('/js', express.static(__dirname + '/client/js'));
app.use('/images', express.static(__dirname + '/client/images'));
app.use(bodyParser.json())

// static route
app.get('/', function(req, res){
  res.sendFile(__dirname + '/client/index.html');
});

app.listen(3000, function(){
  console.log('Listening on port 3000');
});
